function Get-TargetResource
{
	[CmdletBinding()]
	[OutputType([System.Collections.Hashtable])]
	param
	(
		[parameter(Mandatory = $true)]
		[System.String]
		$Filter
	)

	$delegation = "";

    $feature = get-feature $Filter

    if ($feature -ne $null)
    {
        $name = $Filter;
        $delegation = $feature.overrideMode;
    }
	
	
	$returnValue = @{
		Filter = $name
		Delegation = $delegation
	};

	$returnValue;


}




function Set-TargetResource
{
	[CmdletBinding()]
	param
	(
		[parameter(Mandatory = $true)]
		[System.String]
		$Filter,

		[ValidateSet("Allow","Deny")]
		[System.String]
		$Delegation
	)

    $path = get-path;

    if (does-feature-exist $Filter)
    {
        Write-Verbose "Updating feature delegation ('$Name')";
        Set-WebConfiguration -PSPath $path -Filter $Filter -Metadata overrideMode -Value $Delegation;
    }

}




function Test-TargetResource
{
	[CmdletBinding()]
	[OutputType([System.Boolean])]
	param
	(
		[parameter(Mandatory = $true)]
		[System.String]
		$Filter,

		[ValidateSet("Allow","Deny")]
		[System.String]
		$Delegation
	)

	$result = $false;
    $actualDelegation = "";

    $feature = get-feature $Filter;

    if ($feature -ne $null)
    {
        $actualDelegation = $feature.overrideMode;
    }

    if ($actualDelegation -eq $Delegation)
    {
        Write-Verbose "Feature Delegation configuration matches reality...";
        $result = $true;
    }
    else
    {
        Write-Verbose "Feature Delegation configuration does not match reality...";
        
        if ($actualDelegation -ne $Delegation)
        {
            Write-Verbose "Expected delegation to be '$Delegation' but was '$actualDelegation'";
        }
    }
	
	$result;


}


function get-path
{
    return "MACHINE/WEBROOT/APPHOST";
}

function get-feature
{
    param(
        [parameter(Mandatory)]
        [String] $Filter
    )
    
    $path = get-path;
    $feature = (Get-WebConfiguration -PSPath $path -Filter $Filter -Metadata).MetaData | select overrideMode, effectiveOverrideMode, isLocked
    Write-Debug "feature metadata details ...";
    Write-Debug ($feature | Format-List | Out-String);

    return $feature;
}

function does-feature-exist
{
    param(
        [parameter(Mandatory = $true)]
        [String] $Filter
    )

    $feature = get-feature $Filter;

    $exists = if($feature -ne $null) { $true } else { $false }

    Write-Debug "Does feature exist? ... $exists";

    return $exists;
}

Export-ModuleMember -Function *-TargetResource



