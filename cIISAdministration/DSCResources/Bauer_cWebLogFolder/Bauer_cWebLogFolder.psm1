function Get-TargetResource
{
	[CmdletBinding()]
	[OutputType([System.Collections.Hashtable])]
	param
	(
		[parameter(Mandatory = $true)]
		[System.String]
		$Site,

		[parameter(Mandatory = $true)]
		[System.String]
		$Path
	)
	
    $value = "";
    $ensure = "Absent";

    $logFolder = get-log-folder $site $Path

    if ($logFolder -ne $null)
    {
        $Path = $logFolder.Value;
        $Site = $PSBoundParameters.Site;
        $ensure = "Present";
    }
	
	$returnValue = @{
		Path = $Path
		Site = $PSBoundParameters.Site
		Ensure = $ensure
	};

	$returnValue;
}


function Set-TargetResource
{
	[CmdletBinding()]
	param
	(
		[parameter(Mandatory = $true)]
		[System.String]
		$Site,

		[parameter(Mandatory = $true)]
		[System.String]
		$Path,

		[ValidateSet("Present","Absent")]
		[System.String]
		$Ensure
	)
    $psPath = get-path $Site;
	$filter = get-filter $Site;

	if ($Ensure -eq "Present")
    {
		Write-Verbose "Updating log folder '$path' (Site: $Site)";
        Set-ItemProperty "IIS:\Sites\$Site" -name logFile -value @{directory=$Path}
    }
    else
    {
        Write-Verbose "Removing custom header '$Name' (Site: $Site)";
        $filter = "system.applicationHost/sites/siteDefaults/logfile"
        $default = Get-WebConfigurationProperty -PSPath $psPath -Filter $filter -Name "directory"
        Set-ItemProperty "IIS:\Sites\$Site" -name logFile -value @{directory=$default.Value}
    }
}


function Test-TargetResource
{
	[CmdletBinding()]
	[OutputType([System.Boolean])]
	param
	(
		[parameter(Mandatory = $true)]
		[System.String]
		$Site,

		[parameter(Mandatory = $true)]
		[System.String]
		$Path,

		[ValidateSet("Present","Absent")]
		[System.String]
		$Ensure
	)
    $result = $false;
    $actualValue = "";
    $actualEnsure = "Absent";

    $logFolder = get-log-folder $site $Path

    if ($logFolder -ne $null)
    {
        $actualValue = $logFolder.Value;
        $actualEnsure = "Present";
    }

    if ($actualValue -eq $Path `
    -and $actualEnsure -eq $Ensure )
    {
        Write-Verbose "Log folder configuration matches reality...";
        $result = $true;
    }
    else
    {
        Write-Verbose "Log Folder configuration does not match reality...";
        
        if ($actualValue -ne $Value)
        {
            Write-Verbose "Expected value to be '$Value' but was '$actualValue'";
        }

        if ($actualEnsure -ne $Ensure)
        {
            Write-Verbose "Expected state to be '$Ensure' but was '$actualEnsure'";
        }
    }
	
	$result;
}

function get-path
{
    return "MACHINE/WEBROOT/APPHOST";
}

function get-log-folder
{
    param(
        [String] $Site
    )

    $pspath = get-path $Site;

	$filter = get-filter $Site;

    $logFolder = Get-WebConfigurationProperty -PSPath $pspath -Filter $filter -Name directory;

    Write-Debug "Log directory details ...";
    Write-Debug ($logFolder | Format-List | Out-String);

    return $logFolder;
}

function is-log-folder-set
{
    param(
		[parameter(Mandatory)]
        [String] $path,

        [String] $Site
    )

    $logFolder = get-log-folder $Site;
	$exists = $false;

    if(($logFolder -ne $null) -and ( $logFolder.Value -eq $path))
	{ 
		$exists = $true;
	} 
	else 
	{ 
		$exists = $false;
	}

    Write-Debug "Is log folder set? ... $exists";

    return $exists;
}

function get-filter
{
	param(
		[parameter(Mandatory)]
        [String] $site
    )
	return "system.applicationHost/sites/site[@name='$site']/logFile";
}

Export-ModuleMember -Function *-TargetResource

