function Get-TargetResource
{
	[CmdletBinding()]
	[OutputType([System.Collections.Hashtable])]
	param
	(
		[parameter(Mandatory = $true)]
		[ValidateSet("Present","Absent")]
		[System.String]
		$Ensure
	)

    $fields = get-seleted-log-fields
        
    Write-Debug "Currently selected standard fields..."
    Write-Debug ($fields -join ",")
        
	
	$returnValue = @{
		Fields = $fields
		Ensure = "Present"
	}

	$returnValue	



}





function Set-TargetResource
{
	[CmdletBinding()]
	param
	(
		[System.String[]]
		$Fields,

		[parameter(Mandatory = $true)]
		[ValidateSet("Present","Absent")]
		[System.String]
		$Ensure
	)
        
    if ($Fields -eq $null -or $Fields.length -eq 0 -or $Fields[0] -eq "none")
    {
        # No change required
        if ($Ensure -eq "Absent") { return }

        Write-Debug "Array is null, empty or none"
        $Fields = @()
    } 
    elseif($Fields[0] -eq "all")
    {
        Write-Debug "Array is all"
        $Fields = get-all-valid-log-fields
    }
    
    if ($Ensure -ne "Present")
    {
        $currentFields = get-seleted-log-fields
        
        $debug = Write-Debug ($currentFields -join ",")
        Write-Debug "Current Value is $debug"
        Write-Debug "input Value is $Fields"

        $Fields = $currentFields | select-string -pattern $Fields -simplematch -notmatch
    }
        
    $value = $Fields -join ","
    Write-Debug "Value is $value"

    Set-WebConfigurationProperty -PSPath "MACHINE/WEBROOT/APPHOST" -Filter "/system.applicationHost/sites/siteDefaults/logFile" -Name "logExtFileFlags" -Value $value

	#Include this line if the resource requires a system reboot.
	#$global:DSCMachineStatus = 1


}





function Test-TargetResource
{
	[CmdletBinding()]
	[OutputType([System.Boolean])]
	param
	(
		[System.String[]]
		$Fields,

		[parameter(Mandatory = $true)]
		[ValidateSet("Present","Absent")]
		[System.String]
		$Ensure
	)

	$result = $false

    [String[]] $current = get-seleted-log-fields

    Write-Verbose "Input Fields ..."
    Write-Verbose ($Fields -join ",")

    
    if ($Fields -eq $null) 
    { 
        Write-Verbose "Fields is null"
        $Fields = @("none")
    }

    if($Fields[0] -eq "all")
    {
        Write-Debug "Array is all"
        $Fields = get-all-valid-log-fields        
    }
    elseif($Fields[0] -eq "none")
    {
        Write-Debug "No fields"
        $Fields = @()
    }

    $Fields = sanitise-log-fields-array -Fields $Fields

    Write-Verbose "Sanitised Fields ..."
    Write-Verbose ($Fields -join ",")
         
    if ($Fields -eq $null) 
    { 
        Write-Verbose "Fields is empty or null"
        return $true 
    }

    $diff = Compare-Object -ReferenceObject $current -DifferenceObject $Fields -PassThru -IncludeEqual -ExcludeDifferent

    if ($Ensure -eq "Present")
    {
        # All valid items in $fields should be selected 
       $result = $diff.Length -eq $Fields.Length
       Write-Verbose "All required fields ($($Fields -join ",")) are present? ... $result"
    }
    else
    {
       $result = $diff.Length -eq 0
       Write-Verbose "All excluded fields $($Fields -join ",") are absent? ... $result"
    }
    	
	$result

}





Export-ModuleMember -Function *-TargetResource


function get-all-valid-log-fields
{
    return @("Date","Time","ClientIP","UserName","SiteName","ComputerName","ServerIP","Method","UriStem","UriQuery","HttpStatus","Win32Status","BytesSent","BytesRecv","TimeTaken","ServerPort","UserAgent","Cookie","Referer","ProtocolVersion","Host","HttpSubStatus")

}


function sanitise-log-fields-array
{
	[OutputType([System.String[]])]
    param(
        [System.String[]]
		$Fields
    )

    $allValid = get-all-valid-log-fields

    Write-Debug "All..."
    Write-Debug ($allValid -join ",")

    Write-Debug "Before Sanitised..."
    Write-Debug ($Fields -join ",")

    $sanitised = Compare-Object -ReferenceObject $Fields -DifferenceObject $allValid -PassThru -IncludeEqual -ExcludeDifferent
    
    Write-Debug "After Sanitised..."
    Write-Debug ($sanitised -join ",")

    return $sanitised
}

function get-seleted-log-fields
{
    $property = Get-WebConfigurationProperty -PSPath "MACHINE/WEBROOT/APPHOST" -Filter "/system.applicationHost/sites/siteDefaults/logFile" -Name "." | Select logExtFileFlags
    $currentFields = $property.logExtFileFlags -split ","

    return $currentFields
}

