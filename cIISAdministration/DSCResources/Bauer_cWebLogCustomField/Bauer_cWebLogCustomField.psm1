function Get-TargetResource
{
	[CmdletBinding()]
	[OutputType([System.Collections.Hashtable])]
	param
	(
		[parameter(Mandatory = $true)]
		[System.String]
		$FieldName
	)

    $ensure = "Absent"

    $customField = get-custom-log-field $FieldName

    if ($customField -ne $null)
    {
        $sourceType = $customField.sourceType
        $source = $customField.sourceName
        $ensure = "Present"
    }
	
	$returnValue = @{
		FieldName = $FieldName
		SourceType = $sourceType
		Source = $source
		Ensure = $ensure
	}

	$returnValue
	



}





function Set-TargetResource
{
	[CmdletBinding()]
	param
	(
		[parameter(Mandatory = $true)]
		[System.String]
		$FieldName,

		[ValidateSet("RequestHeader","ResponseHeader","ServerVariable")]
		[System.String]
		$SourceType,

		[System.String]
		$Source,

		[ValidateSet("Present","Absent")]
		[System.String]
		$Ensure
	)

    if ($Ensure -eq "Present")
    {
        if (does-custom-log-field-exist $FieldName)
        {
            Write-Verbose "Updating custom log field $FieldName"
            Set-WebConfigurationProperty -PSPath "MACHINE/WEBROOT/APPHOST" -Filter "/system.applicationHost/sites/siteDefaults/logFile/customFields" -Name Collection[logFieldName="$FieldName"] -Value @{logFieldName=$FieldName;sourceType=$SourceType;sourceName=$Source}
        }
        else
        {
            Write-Verbose "Adding custom log field $FieldName"
            Add-WebConfigurationProperty -PSPath "MACHINE/WEBROOT/APPHOST" -Filter "/system.applicationHost/sites/siteDefaults/logFile/customFields" -Name "." -Value @{logFieldName=$FieldName;sourceType=$SourceType;sourceName=$Source}   
        }
    }
    else
    {
        Write-Verbose "Removing custom log field $FieldName"
        Remove-WebConfigurationProperty -PSPath "MACHINE/WEBROOT/APPHOST" -Filter "/system.applicationHost/sites/siteDefaults/logFile/customFields" -Name Collection[logFieldName="$FieldName"]
    }

	#Include this line if the resource requires a system reboot.
	#$global:DSCMachineStatus = 1


}





function Test-TargetResource
{
	[CmdletBinding()]
	[OutputType([System.Boolean])]
	param
	(
		[parameter(Mandatory = $true)]
		[System.String]
		$FieldName,

		[ValidateSet("RequestHeader","ResponseHeader","ServerVariable")]
		[System.String]
		$SourceType,

		[System.String]
		$Source,

		[ValidateSet("Present","Absent")]
		[System.String]
		$Ensure
	)

	
	$result = $false
    $actualEnsure = "Absent"

    $customField = get-custom-log-field $FieldName

    if ($customField -ne $null)
    {
        $actualFieldName = $customField.logFieldName
        $actualSourceType = $customField.sourceType
        $actualSource = $customField.sourceName
        $actualEnsure = "Present"
    }

    if ($actualFieldName -eq $FieldName `
    -and $actualSourceType -eq $SourceType `
    -and $actualSource -eq $Source `
    -and $actualEnsure -eq $Ensure )
    {
        Write-Verbose "Custom log field configuration matches reality..."
        $result = $true
    }
    else
    {
        Write-Verbose "Custom log field configuration does not match reality..."

        if ($actualFieldName -eq $FieldName)
        {
            Write-Verbose "Expected field name to be $FieldName but was $actualFieldName"
        }

        if ($actualSourceType -eq $SourceType)
        {
            Write-Verbose "Expected source type to be $SourceType but was $actualSourceType"
        }

        if ($actualSource -eq $Source)
        {
            Write-Verbose "Expected source to be $Source but was $actualSource"
        }

        if ($actualEnsure -eq $Ensure)
        {
            Write-Verbose "Expected state to be $Ensure but was $actualEnsure"
        }
    }

	
	$result
	



}


function get-custom-log-field
{
    param(
        [parameter(Mandatory = $true)]
        [System.String]
        $FieldName
    )

    $customField = Get-WebConfigurationProperty -PSPath "MACHINE/WEBROOT/APPHOST" -Filter "/system.applicationHost/sites/siteDefaults/logFile/customFields" -Name Collection[logFieldName="$FieldName"] | select logFieldName,sourceType,sourceName

    Write-Debug "Custom log field details ..."
    Write-Debug ($customField | Format-List | Out-String)

    return $customField
}

function does-custom-log-field-exist
{
    param(
        [parameter(Mandatory = $true)]
        [System.String]
        $FieldName
    )

    $customField = get-custom-log-field $FieldName

    $exists = if($customField -ne $null) { $true } else { $false }

    Write-Debug "Does custom log field exist? ... $exists"

    return $exists
}



Export-ModuleMember -Function *-TargetResource

