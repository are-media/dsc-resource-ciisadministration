function Get-TargetResource
{
	[CmdletBinding()]
	[OutputType([System.Collections.Hashtable])]
	param
	(
		[parameter(Mandatory = $true)]
		[System.String]
		$Name
	)

    $value = "";
    $ensure = "Absent";

    $customHeader = get-custom-header $Name $PSBoundParameters.Site

    if ($customHeader -ne $null)
    {
        $name = $customHeader.name;
        $value = $customHeader.value;
        $ensure = "Present";
    }
	
	
	$returnValue = @{
		Name = $name
		Value = $value
		Site = $PSBoundParameters.Site
		Ensure = $ensure
	};

	$returnValue;
	
}


function Set-TargetResource
{
	[CmdletBinding()]
	param
	(
		[parameter(Mandatory = $true)]
		[System.String]
		$Name,

		[System.String]
		$Value,

		[System.String]
		$Site,

		[ValidateSet("Present","Absent")]
		[System.String]
		$Ensure
	)

    $path = get-path $Site;

	if ($Ensure -eq "Present")
    {
        $valueData = @{
            name=$Name
            value=$Value
        };

        if (does-custom-header-exist $Name $Site)
        {
            Write-Verbose "Updating custom header '$Name' (Site: $Site)";
            Set-WebConfigurationProperty -PSPath $path -Filter $Filter -Name Collection[name="$Name"] -Value $valueData;
        }
        else
        {
            Write-Verbose "Adding custom header '$Name' (Site: $Site)";
            Add-WebConfigurationProperty -PSPath $path -Filter $Filter -Name "." -Value $valueData;
        }
    }
    else
    {
        Write-Verbose "Removing custom header '$Name' (Site: $Site)";
        Remove-WebConfigurationProperty -PSPath $path -Filter $Filter -Name Collection[name="$Name"];
    }
}


function Test-TargetResource
{
	[CmdletBinding()]
	[OutputType([System.Boolean])]
	param
	(
		[parameter(Mandatory = $true)]
		[System.String]
		$Name,

		[System.String]
		$Value,

		[System.String]
		$Site,

		[ValidateSet("Present","Absent")]
		[System.String]
		$Ensure
	)

    $result = $false;
    $actualValue = "";
    $actualEnsure = "Absent";

    $customHeader = get-custom-header $Name $Site;

    if ($customHeader -ne $null)
    {
        $actualValue = $customHeader.value;
        $actualEnsure = "Present";
    }

    if ($actualValue -eq $Value `
    -and $actualEnsure -eq $Ensure )
    {
        Write-Verbose "Custom header configuration matches reality...";
        $result = $true;
    }
    else
    {
        Write-Verbose "Custom header configuration does not match reality...";
        
        if ($actualValue -ne $Value)
        {
            Write-Verbose "Expected value to be '$Value' but was '$actualValue'";
        }

        if ($actualEnsure -ne $Ensure)
        {
            Write-Verbose "Expected state to be '$Ensure' but was '$actualEnsure'";
        }
    }
	
	$result;
}

function get-path
{
    param(
        [String] $Site
    )
    
    $path = "MACHINE/WEBROOT/APPHOST";

    if ($Site -ne "")
    {
        $path += "/$Site";
    }

    return $path;
}

function get-custom-header
{
    param(
        [parameter(Mandatory)]
        [String] $Name,

        [String] $Site
    )

    $path = get-path $Site;

    $customHeader = Get-WebConfigurationProperty -PSPath $path -Filter $Filter -Name Collection[name="$Name"] | select name,value;

    Write-Debug "Custom header details ...";
    Write-Debug ($customHeader | Format-List | Out-String);

    return $customHeader;
}

function does-custom-header-exist
{
    param(
        [parameter(Mandatory = $true)]
        [String] $Name,

        [String] $Site
    )

    $customHeader = get-custom-header $Name $Site;

    $exists = if($customHeader -ne $null) { $true } else { $false }

    Write-Debug "Does custom header exist? ... $exists";

    return $exists;
}

$Filter = "system.WebServer/httpProtocol/customHeaders"

Export-ModuleMember -Function *-TargetResource

