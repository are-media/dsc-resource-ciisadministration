﻿$moduleName = "cIISAdministration"
$powershellPath = "C:\Program Files\WindowsPowerShell\Modules"
$modulePath = Join-Path $powershellPath $moduleName
$moduleDscPath = Join-Path $modulePath "DSCResources"


#### Setup cWebLogCustomField #####
$resourceFriendlyName = "cFeatureDelegation"
$resourceName = "Bauer_$resourceFriendlyName"
$resoucePath = Join-Path $moduleDscPath $resourceName


$filter = New-xDscResourceProperty  -Name "Filter"  -Type String   -Attribute Key      -Description "Filter path of section in web.config"

$delegation = New-xDscResourceProperty -Name "Delegation" -Type String   -Attribute Write    -Description "Type of the source of the custom field" -ValidateSet @("Allow","Deny")

$properties = @($filter,$delegation)

update-dsc-resource $resoucePath $resourceName $properties $powershellPath $moduleName $resourceFriendlyName

#### Setup cWebLogStandardFields #####
$resourceFriendlyName = "cWebLogStandardFields"
$resourceName = "Bauer_$resourceFriendlyName"
$resoucePath = Join-Path $moduleDscPath $resourceName

$fields = New-xDscResourceProperty -Name "Fields" -Type String[] -Attribute Write -Description "List of standard log fields"

$ensure = New-xDscResourceProperty -Name "Ensure" -Type String   -Attribute Key   -Description "Whether the log fields should be present or not" -ValidateSet @("Present", "Absent")

$properties = @($fields, $ensure)

update-dsc-resource $resoucePath $resourceName $properties $powershellPath $moduleName $resourceFriendlyName

#### Setup cWebLogCustomField #####
$resourceFriendlyName = "cWebLogCustomField"
$resourceName = "Bauer_$resourceFriendlyName"
$resoucePath = Join-Path $moduleDscPath $resourceName


$fieldName = New-xDscResourceProperty  -Name "FieldName"  -Type String   -Attribute Key      -Description "Name of the custom field"

$sourceType = New-xDscResourceProperty -Name "SourceType" -Type String   -Attribute Write    -Description "Type of the source of the custom field" -ValidateSet @("RequestHeader","ResponseHeader", "ServerVariable")

$source = New-xDscResourceProperty     -Name "Source"     -Type String   -Attribute Write    -Description "Source of the custom field"

$ensure = New-xDscResourceProperty     -Name "Ensure"     -Type String   -Attribute Write    -Description "Whether a log field should be present or not" -ValidateSet @("Present", "Absent")

$properties = @($fieldName,$sourceType, $source, $ensure)

update-dsc-resource $resoucePath $resourceName $properties $powershellPath $moduleName $resourceFriendlyName

### Setup cWebLogFolder #####
$resourceFriendlyName = "cWebLogFolder"
$resourceName = "Bauer_$resourceFriendlyName"
$resoucePath = Join-Path $moduleDscPath $resourceName


$Site = New-xDscResourceProperty   -Name "Site"   -Type String   -Attribute Key      -Description "Site custom header should apply to"

$Path = New-xDscResourceProperty   -Name "Path"   -Type String   -Attribute Required      -Description "Log File Path"

$Ensure = New-xDscResourceProperty -Name "Ensure" -Type String   -Attribute Write    -Description "Whether the custom header should be present or not" -ValidateSet @("Present", "Absent")

$properties = @($Site, $Path,  $Ensure)

update-dsc-resource $resoucePath $resourceName $properties $powershellPath $moduleName $resourceFriendlyName


### Setup cWebCustomHeaders #####
$resourceFriendlyName = "cWebCustomHeaders"
$resourceName = "Bauer_$resourceFriendlyName"
$resoucePath = Join-Path $moduleDscPath $resourceName


$Name = New-xDscResourceProperty   -Name "Name"   -Type String   -Attribute Key      -Description "Name of the custom header"

$Value = New-xDscResourceProperty  -Name "Value"  -Type String   -Attribute Write    -Description "Value of the custom header"

$Site = New-xDscResourceProperty   -Name "Site"   -Type String   -Attribute Write    -Description "Site custom header should apply to"

$Ensure = New-xDscResourceProperty -Name "Ensure" -Type String   -Attribute Write    -Description "Whether the custom header should be present or not" -ValidateSet @("Present", "Absent")

$properties = @($Name, $Value, $Site, $Ensure)

update-dsc-resource $resoucePath $resourceName $properties $powershellPath $moduleName $resourceFriendlyName

function update-dsc-resource
{
    param(
        [string]$resoucePath,
        [string]$resourceName,
        [array] $properties,
        [string]$powershellPath,
        [string]$moduleName,
        [string]$resourceFriendlyName
    )

    Write-Host "resource path is $resoucePath"

    if (Test-Path $resoucePath)
    {
        Write-Host "Updating resource $resourceName"
        Update-xDscResource -Name $resourceName -Property $properties -Force
    }
    else
    {
        Write-Host "Creating new resource $resourceName"
        New-xDscResource -Name $resourceName -Property $properties -Path $powershellPath -ModuleName $moduleName -FriendlyName $resourceFriendlyName -Force
    }
}